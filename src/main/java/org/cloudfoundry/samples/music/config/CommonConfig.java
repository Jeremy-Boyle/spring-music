
package org.cloudfoundry.samples.music.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@ConfigurationProperties(prefix = "common")
public class CommonConfig {

    private String message;
    private String environment;
    private String smtpServer;
    private String smtpUser;
    private String smtpPassword;

    
    public String getMessage(){
      return this.message;
    }

    public String setMessage(String message){
      return this.message = message;
    }

    public String getEnvironment(){
      return this.environment;
    }

     public String setEnvironment(String environment){
      return this.environment = environment;
    }

    public String getSmtpServer(){
      return this.smtpServer;
    }

    public String setSmtpServer(String smtpServer){
      return this.smtpServer = smtpServer;
    }

    public String getSmtpUser(){
      return this.smtpUser;
    }

    public String setSmtpUser(String smtpUser){
      return this.smtpUser = smtpUser;
    }

     public String getSmtpPassword(){
      return this.smtpPassword;
    }

    public String setSmtpPassword(String smtpPassword){
      return this.smtpPassword = smtpPassword;
    }

}
